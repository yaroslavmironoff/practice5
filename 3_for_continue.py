# Напишите программу, которая выводит все числа от 1 до 100,
# кроме чисел 7, 17, 29 и 78, используйте continue

for i in range(1, 101):
    if i == 7 or i == 17 or i == 29 or i == 78:
        continue
    else:
        print(i)
